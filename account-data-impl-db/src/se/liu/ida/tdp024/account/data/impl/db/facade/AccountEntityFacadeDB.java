package se.liu.ida.tdp024.account.data.impl.db.facade;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.Query;
import javax.persistence.RollbackException;
import se.liu.ida.tdp024.account.data.api.entity.Account;
import se.liu.ida.tdp024.account.data.api.entity.Transaction;
import se.liu.ida.tdp024.account.data.api.facade.AccountEntityFacade;
import se.liu.ida.tdp024.account.data.impl.db.entity.AccountDB;
import se.liu.ida.tdp024.account.data.impl.db.util.EMF;
import se.liu.ida.tdp024.account.util.logger.AccountLogger;
import se.liu.ida.tdp024.account.util.logger.AccountLoggerImpl;
import se.liu.ida.tdp024.account.util.logger.AccountLoggerMonlog;

public class AccountEntityFacadeDB implements AccountEntityFacade {

    private static final AccountLogger accountLogger = new AccountLoggerMonlog();
    
    @Override
    public long create(String accountType, String personKey, String bankKey) {
        EntityManager em = EMF.getEntityManager();

        try{
            em.getTransaction().begin();
            
            Account account = new AccountDB();
            account.setAccountType(accountType);
            account.setPersonKey(personKey);
            account.setBankKey(bankKey);
                        
            em.persist(account);
            
            em.getTransaction().commit();
            
            return account.getId();
        }catch(Exception e){
            accountLogger.log(e);
            return 0;
        }finally {

            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }

            em.close();

        }
    }

    @Override
    public List<Account> find(String key) {
        EntityManager em = EMF.getEntityManager();

        try{
            
            Query query = em.createQuery("SELECT a FROM AccountDB a WHERE a.personKey = :key");
            query.setParameter("key", key);

            List<Account> results = query.getResultList();

            return results;
        }
        catch(Exception e){
            accountLogger.log(e);
            return null;            
        } 
        finally {
            em.close();
        }
    }

    @Override
    public String debit(long accountID, int amount) {
            EntityManager em = EMF.getEntityManager();

        try{
            em.getTransaction().begin();

            Account account = em.find(AccountDB.class, accountID);

            if (account == null) {
                
                String message = "Could not find a account object with id: " + accountID;
                accountLogger.log(AccountLogger.TodoLoggerLevel.DEBUG, message, message);
                return "FAILED";
            }
            em.lock(account, LockModeType.PESSIMISTIC_WRITE);
            int newHoldings = account.getHoldings() - amount;
            if(newHoldings < 0){
                String message = "Holdings are to low for the transaction";
                accountLogger.log(AccountLogger.TodoLoggerLevel.DEBUG, message, message);
                return "FAILED";
            }
            
            account.widthdrawHoldings(amount);
                        /* Similar reasoning as with persist in create method */
            em.merge(account);

            /* Similar reasoning as in create method */
            em.getTransaction().commit();
            return "OK";

        } catch (Exception e) {

            accountLogger.log(e);


           return "FAILED";
        } finally {
            
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
            em.close();
        }
        
    }

    @Override
    public String credit(long accountID, int amount) {
        EntityManager em = EMF.getEntityManager();

        try{
            em.getTransaction().begin();
            Account account = em.find(AccountDB.class, accountID);

            if (account == null) {
                
                String message = "Could not find a account object with id: " + accountID;
                accountLogger.log(AccountLogger.TodoLoggerLevel.DEBUG, message, message);
                return "FAILED";
            }
            em.lock(account, LockModeType.PESSIMISTIC_WRITE);

            account.depositHoldings(amount);
            em.merge(account);

            /* Similar reasoning as in create method */
            em.getTransaction().commit();
            return "OK";

        } catch (Exception e) {

           accountLogger.log(e);
           return "FAILED";
        } finally {
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
            em.close();
        }    
    }    
}

