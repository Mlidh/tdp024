package se.liu.ida.tdp024.account.data.impl.db.entity;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import se.liu.ida.tdp024.account.data.api.entity.Account;
import se.liu.ida.tdp024.account.data.api.entity.Transaction;

@Entity
public class AccountDB implements Account {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    
    private String bankKey;
    private String accountType;
    private String personKey;
    private int holdings;
    
    @OneToMany(mappedBy = "account", targetEntity = TransactionDB.class)
    private List<Transaction> transactions;

    @Override
    public void setAccountType(String type) {
        this.accountType = type;
    }

    @Override
    public long getId() {
        return this.id;
    }

    @Override
    public String getAccountType() {
        return this.accountType;
    }

    @Override
    public String getPersonKey() {
        return this.personKey;
    }

    @Override
    public void setPersonKey(String personKey) {
        this.personKey = personKey;
    }

    @Override
    public String getBankKey() {
        return this.bankKey;
    }

    @Override
    public void setBankKey(String bankKey) {
        this.bankKey = bankKey;
    }

    @Override
    public int getHoldings() {
        return this.holdings;
    }


    @Override
    public void widthdrawHoldings(int holdings) {
        this.holdings -= holdings;
    }

    @Override
    public void depositHoldings(int holdings) {
        this.holdings += holdings;
    }
    
    
}
