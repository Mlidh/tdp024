/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.liu.ida.tdp024.account.data.impl.db.facade;

import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.Query;
import org.eclipse.persistence.jpa.internal.jpql.parser.DateTime;
import se.liu.ida.tdp024.account.data.api.entity.Account;
import se.liu.ida.tdp024.account.data.api.entity.Transaction;
import se.liu.ida.tdp024.account.data.api.facade.TransactionEntityFacade;
import se.liu.ida.tdp024.account.data.impl.db.entity.AccountDB;
import se.liu.ida.tdp024.account.data.impl.db.entity.TransactionDB;
import se.liu.ida.tdp024.account.data.impl.db.util.EMF;
import se.liu.ida.tdp024.account.util.logger.AccountLogger;
import se.liu.ida.tdp024.account.util.logger.AccountLoggerMonlog;

/**
 *
 * @author Klante
 */
public class TransactionEntityFacadeDB implements TransactionEntityFacade{
    private static final AccountLogger accountLogger = new AccountLoggerMonlog();

    private static String getCurrentTimeStamp() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }
    
    @Override
    public long create(String type, int amount, String status, long accountID) {
          EntityManager em = EMF.getEntityManager();

        try{
            em.getTransaction().begin();      
            
            Account account = em.find(AccountDB.class, accountID);

            if(account == null){
                String message = "Could not find a account object with id: " + accountID;
                accountLogger.log(AccountLogger.TodoLoggerLevel.DEBUG, message, message);
                return -1;                        
            }
            
            Transaction transaction = new TransactionDB();
            transaction.setType(type);
            
                      
            transaction.setCreated(getCurrentTimeStamp());
            transaction.setAmount(amount);
            transaction.setStatus(status);
            transaction.setAccount(account);
            
            em.persist(transaction);
            
            em.getTransaction().commit();
            return transaction.getId();
            
        }catch(Exception e){
            accountLogger.log(e);
            return -1;
        }finally {

            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }

            em.close();

        }
        
    }

    @Override
    public List<Transaction> findAllByAccount(long accountID) {
        EntityManager em = EMF.getEntityManager();

        try{
            
            Account account = em.find(AccountDB.class, accountID);

            Query query = em.createQuery("SELECT t FROM TransactionDB t WHERE t.account = :account");
            query.setParameter("account", account);

            List<Transaction> results = query.getResultList();

            return results;
        }
        catch(Exception e){
            accountLogger.log(e);
            return null;            
        } 
        finally {
            em.close();
        }
    }

    
    
}
