/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.liu.ida.tdp024.account.logic.impl.facade;

import se.liu.ida.tdp024.account.data.impl.db.entity.JsonHolderDB;
import se.liu.ida.tdp024.account.logic.api.facade.BankLogicFacade;
import se.liu.ida.tdp024.account.util.http.HTTPHelper;
import se.liu.ida.tdp024.account.util.http.HTTPHelperImpl;
import se.liu.ida.tdp024.account.util.json.AccountJsonSerializer;
import se.liu.ida.tdp024.account.util.json.AccountJsonSerializerImpl;
import se.liu.ida.tdp024.account.util.logger.AccountLogger;
import se.liu.ida.tdp024.account.util.logger.AccountLoggerMonlog;

/**
 *
 * @author Klante
 */
public class BankLogicFacadeImpl implements BankLogicFacade{

    private HTTPHelper httpHelper  = new HTTPHelperImpl();
    private String urlBase = "http://enterprise-systems.appspot.com/bank";
    private AccountJsonSerializer jsonSerializer = new AccountJsonSerializerImpl();
    private AccountLogger logger = new AccountLoggerMonlog();

    @Override
    public String getBankKey(String bankName) {
        try{
            String bankJson = httpHelper.get(urlBase + "/find.name","name",bankName); 
            JsonHolderDB jsonh = jsonSerializer.fromJson(bankJson, JsonHolderDB.class);
            return jsonh.getKey();
        }catch(Exception e){
            logger.log(e);
            return null;
        }
    }
    
}
