package se.liu.ida.tdp024.account.logic.impl.facade;

import java.util.List;
import se.liu.ida.tdp024.account.data.api.entity.Account;
import se.liu.ida.tdp024.account.data.api.entity.Transaction;
import se.liu.ida.tdp024.account.data.api.facade.AccountEntityFacade;
import se.liu.ida.tdp024.account.data.api.facade.TransactionEntityFacade;
import se.liu.ida.tdp024.account.logic.api.facade.AccountLogicFacade;
import se.liu.ida.tdp024.account.logic.api.facade.BankLogicFacade;
import se.liu.ida.tdp024.account.logic.api.facade.PersonLogicFacade;

public class AccountLogicFacadeImpl implements AccountLogicFacade {
    
    private AccountEntityFacade accountEntityFacade;
    private TransactionEntityFacade transactionEntityFacade;

    private BankLogicFacade bankLogic = new BankLogicFacadeImpl();
    private PersonLogicFacade personLogic = new PersonLogicFacadeImpl();

    
    public AccountLogicFacadeImpl(AccountEntityFacade accountEntityFacade, 
            TransactionEntityFacade transactionEntityFacade) {
        this.accountEntityFacade = accountEntityFacade;
        this.transactionEntityFacade = transactionEntityFacade;
    }

    @Override
    public long create(String type, String name, String bank) {
        
        String bankKey = bankLogic.getBankKey(bank);
        String personKey = personLogic.getPersonKey(name);
        
        if(type == null || personKey == null || personKey.isEmpty() 
                || bankKey == null || bankKey.isEmpty()){
            return 0;
        }
        
        if(type.equals("SAVINGS") || type.equals("CHECK")){
            return accountEntityFacade.create(type, personKey, bankKey);
        }else{
            return 0;
        }
                        
    }

    @Override
    public List<Account> find(String name) {
        
        String personKey = personLogic.getPersonKey(name);

        return accountEntityFacade.find(personKey);

        
    }

    @Override
    public String debit(long account, int amount) {
        
        String status = accountEntityFacade.debit(account, amount);
        long id = transactionEntityFacade.create("DEBIT", amount, status, account);
        if(id < 0){
            return "FAILED";
        }
        
        return status;
    }

    @Override
    public String credit(long account, int amount) {
        String status = accountEntityFacade.credit(account, amount);
        long id = transactionEntityFacade.create("CREDIT", amount, status, account);
        
        if(id < 0){
            return "FAILED";
        }
        
        return status;    
    }

    @Override
    public List<Transaction> transactions(long account) {
        
        return transactionEntityFacade.findAllByAccount(account);        
        
    }

    
}
