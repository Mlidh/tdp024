/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.liu.ida.tdp024.account.data.test.facade;

import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import se.liu.ida.tdp024.account.data.api.entity.Transaction;
import se.liu.ida.tdp024.account.data.api.facade.AccountEntityFacade;
import se.liu.ida.tdp024.account.data.api.facade.TransactionEntityFacade;
import se.liu.ida.tdp024.account.data.api.util.StorageFacade;
import se.liu.ida.tdp024.account.data.impl.db.facade.AccountEntityFacadeDB;
import se.liu.ida.tdp024.account.data.impl.db.facade.TransactionEntityFacadeDB;
import se.liu.ida.tdp024.account.data.impl.db.util.StorageFacadeDB;

/**
 *
 * @author Klante
 */
public class TransactionEntityFacadeTest {
    private TransactionEntityFacade transactionEntityFacade = new TransactionEntityFacadeDB();
    private AccountEntityFacade accountEntityFacade = new AccountEntityFacadeDB();

    private StorageFacade storageFacade = new StorageFacadeDB();
    
    @After
    public void tearDown() {
        storageFacade.emptyStorage();
    }
    
    @Test
    public void testCreate() {        
        long id = accountEntityFacade.create("SAVINGS","nyckel","banknyckel");
        long idT = transactionEntityFacade.create("DEBIT",200,"OK",id);
        Assert.assertFalse(idT == 0);         
    }
    
    @Test
    public void testFindAllByAccount(){
        long id = accountEntityFacade.create("SAVINGS","nyckel","banknyckel");

        {
            long idT = transactionEntityFacade.create("DEBIT",200,"OK",id);
            long idT2 = transactionEntityFacade.create("CREDIT",200,"FAILED",id);
            
            List<Transaction> transactions = transactionEntityFacade.findAllByAccount(id);
            Assert.assertEquals(transactions.get(0).getAmount(), 200);
            Assert.assertEquals(transactions.get(0).getStatus(), "OK");
            Assert.assertEquals(transactions.get(0).getId(),idT);
            Assert.assertEquals(transactions.get(0).getType(),"DEBIT");
            Assert.assertEquals(transactions.get(0).getAccount().getId(),id);
            
            Assert.assertEquals(transactions.get(1).getAmount(), 200);
            Assert.assertEquals(transactions.get(1).getStatus(), "FAILED");
            Assert.assertEquals(transactions.get(1).getId(),idT2);
            Assert.assertEquals(transactions.get(1).getType(),"CREDIT");
            Assert.assertEquals(transactions.get(1).getAccount().getId(),id);

        }
        
    }

}
