package se.liu.ida.tdp024.account.data.test.facade;

import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import se.liu.ida.tdp024.account.data.api.entity.Account;
import se.liu.ida.tdp024.account.data.api.facade.AccountEntityFacade;
import se.liu.ida.tdp024.account.data.api.util.StorageFacade;
import se.liu.ida.tdp024.account.data.impl.db.facade.AccountEntityFacadeDB;
import se.liu.ida.tdp024.account.data.impl.db.util.StorageFacadeDB;

public class AccountEntityFacadeTest {
    
    //---- Unit under test ----//
    private AccountEntityFacade accountEntityFacade = new AccountEntityFacadeDB();

    private StorageFacade storageFacade = new StorageFacadeDB();
    
    @After
    public void tearDown() {
        storageFacade.emptyStorage();
    }
    
    @Test
    public void testCreate() {        
        //Successfull create
        long id = accountEntityFacade.create("SAVINGS","nyckel","banknyckel");
        Assert.assertFalse(id == 0);
         
        //Excaption
        {
            String longCrash = "DEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBITDEBIT";
            id = accountEntityFacade.create(longCrash,"nyckel","Banknyckel");
            Assert.assertTrue(id == 0);
        
        }
    }
    
    @Test
    public void testFind(){                
        String nyckel = "asdasdd";
        String bank = "banknycklel";
        long id = accountEntityFacade.create("SAVINGS",nyckel,bank);
        {
            List<Account> accounts = accountEntityFacade.find(nyckel);
            Assert.assertNotNull(accounts);
            Assert.assertEquals(nyckel, accounts.get(0).getPersonKey());
            Assert.assertEquals(bank, accounts.get(0).getBankKey());
            Assert.assertEquals("SAVINGS",accounts.get(0).getAccountType());
        }
        
    }
    
    @Test
    public void testCreditAndDebit(){
        String nyckel = "asdaasdasdadssdd";
        String bank = "asdadsad";
        int amount = 200;
        long id = accountEntityFacade.create("SAVINGS",nyckel,bank);
        {
            String status = accountEntityFacade.credit(id, amount);
            Assert.assertEquals("OK",status);
            String statusd = accountEntityFacade.debit(id, amount);
            Assert.assertEquals("OK",statusd);
        }
        {
            String status = accountEntityFacade.credit(id, 10);
            Assert.assertEquals("OK",status);
            String statusd = accountEntityFacade.debit(id, 10000);
            Assert.assertEquals("FAILED",statusd);
        }
        {
            String statusFailC = accountEntityFacade.credit(-200, 200);
            Assert.assertEquals("FAILED", statusFailC);
            String statusFail = accountEntityFacade.debit(-200, 200);
            Assert.assertEquals("FAILED", statusFail);
        }
        
    }
        
   
}