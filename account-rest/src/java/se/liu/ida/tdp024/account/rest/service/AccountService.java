package se.liu.ida.tdp024.account.rest.service;

import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import se.liu.ida.tdp024.account.data.api.entity.Account;
import se.liu.ida.tdp024.account.data.api.entity.Transaction;
import se.liu.ida.tdp024.account.data.impl.db.entity.AccountDB;
import se.liu.ida.tdp024.account.data.impl.db.facade.AccountEntityFacadeDB;
import se.liu.ida.tdp024.account.data.impl.db.facade.TransactionEntityFacadeDB;
import se.liu.ida.tdp024.account.logic.api.facade.AccountLogicFacade;
import se.liu.ida.tdp024.account.logic.impl.facade.AccountLogicFacadeImpl;
import se.liu.ida.tdp024.account.util.json.AccountJsonSerializer;
import se.liu.ida.tdp024.account.util.json.AccountJsonSerializerImpl;
import se.liu.ida.tdp024.account.util.logger.AccountLogger;
import se.liu.ida.tdp024.account.util.logger.AccountLoggerImpl;

@Path("/account")
public class AccountService {

    private final AccountLogicFacade accountLogicFacade = 
            new AccountLogicFacadeImpl(new AccountEntityFacadeDB(), new TransactionEntityFacadeDB());
    
    private static final AccountLogger accountLogger = new AccountLoggerImpl();
    private static final AccountJsonSerializer jsonSerializer = new AccountJsonSerializerImpl();
    
    @GET
    @Path("create")
    public Response create(
            @QueryParam("accounttype") String type,
            @QueryParam("name") String name,
            @QueryParam("bank") String bank){
        
        long id = accountLogicFacade.create(type, name, bank);
        if(id > 0){
            return Response.status(Response.Status.OK).entity("OK").build();
        }
        else{
            return Response.status(Response.Status.OK).entity("FAILED").build();
        }
    }
    
    @GET
    @Path("find/name")
    public Response findByName(
            @QueryParam("name") String name){
        
        List<Account>acc = accountLogicFacade.find(name);
        
        String json = jsonSerializer.toJson(acc);
        return Response.status(Response.Status.OK).entity(json).build(); 
    }
    
    @GET
    @Path("debit")
    public Response debit(
            @QueryParam("id") long id,
            @QueryParam("amount") int amount){
        
        String status = accountLogicFacade.debit(id, amount);
        
        return Response.status(Response.Status.OK).entity(status).build();
    }
    
    @GET
    @Path("credit")
    public Response credit(
            @QueryParam("id") long id,
            @QueryParam("amount") int amount){
        
        String status = accountLogicFacade.credit(id, amount);
        
        return Response.status(Response.Status.OK).entity(status).build();    }
    
    @GET
    @Path("transactions")
    public Response transactions(
            @QueryParam("id") long accountID){
        
        List<Transaction> transactions = accountLogicFacade.transactions(accountID);
        String json = jsonSerializer.toJson(transactions);

        return Response.status(Response.Status.OK).entity(json).build();
    }
}
