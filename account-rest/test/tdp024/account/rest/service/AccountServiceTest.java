package tdp024.account.rest.service;

import javax.ws.rs.core.Response;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import se.liu.ida.tdp024.account.data.api.util.StorageFacade;
import se.liu.ida.tdp024.account.data.impl.db.util.StorageFacadeDB;
import se.liu.ida.tdp024.account.rest.service.AccountService;
import se.liu.ida.tdp024.account.util.http.HTTPHelper;
import se.liu.ida.tdp024.account.util.http.HTTPHelperImpl;

public class AccountServiceTest {

    //-- Units under test ---//
    private static final HTTPHelper httpHelper = new HTTPHelperImpl();
    private AccountService accountService = new AccountService();
    private StorageFacade storageFacade = new StorageFacadeDB();
    private String URL = "http://localhost:8080/account-rest/";
    @After
    public void tearDown() {
        storageFacade.emptyStorage();

    }

    @Test
    public void testCreate() {
        
        {
            String name = "Anders Andersson";
            String bank = "SWEDBANK";
            String accountType = "SAVINGS";
            Response response = accountService.create(accountType, name, bank);
            Assert.assertEquals("OK", response.getEntity());
        }
        
        {
            String name = "Jag är intgen person";
            String bank = "SWEDBANK";
            String accountType = "SAVINGS";
            Response response = accountService.create(accountType, name, bank);
            Assert.assertEquals("FAILED", response.getEntity());
        }
        
        {
            String name = "Anders Andersson";
            String bank = "INGENBANK";
            String accountType = "SAVINGS";
            Response response = accountService.create(accountType, name, bank);
            Assert.assertEquals("FAILED", response.getEntity());
        }
    }
    
    @Test
    public void testFind(){
        
        {
            Response response = accountService.findByName("Bertil Bertilsson");
            Assert.assertEquals(response.getEntity(), "[]");
        }
        
        {
            String name = "Bertil Bertilsson";
            String bank = "SWEDBANK";
            String accountType = "SAVINGS";
            Response res = accountService.create(accountType, name, bank);
            Assert.assertEquals("OK", res.getEntity());
            Response response = accountService.findByName("Bertil Bertilsson");
            
            String obj = "[{\"id\":1,\"bankKey\":\"ahRzfmVudGVycHJpc2Utc3lzdGVtc3ILCxIEQmFuaxiJJww\",\"accountType\":\"" +accountType +"\",\"personKey\":\"ahRzfmVudGVycHJpc2Utc3lzdGVtc3INCxIGUGVyc29uGOFdDA\",\"holdings\":0}]";
            
            Assert.assertEquals(response.getEntity(), obj);
            
        }
        
    }
    @Test
    public void testDebitAndCredit()
    {
        String name = "Bertil Bertilsson";
        String bank = "SWEDBANK";
        String accountType = "SAVINGS";
        accountService.create(accountType, name, bank);

        {
            Response res = accountService.credit(1, 40);
            Assert.assertEquals("OK", res.getEntity());
            Response res2 = accountService.debit(1, 40);
            Assert.assertEquals("OK", res2.getEntity());
            Response res3 = accountService.debit(1, 200);
            Assert.assertEquals("FAILED", res3.getEntity());
        }
        
        
    }
    
    
    @Test
    public void testTransactions(){
        
        {
            Response res = accountService.transactions(-200);
            Assert.assertEquals(res.getEntity(), "[]");
        }        
        
    }
}