/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.liu.ida.tdp024.account.data.api.entity;

/**
 *
 * @author Klante
 */
public interface JsonHolder {
    
    public String getKey();
    public String getName();
    
    public void setKey(String key);
    public void setName(String name);
    
}
