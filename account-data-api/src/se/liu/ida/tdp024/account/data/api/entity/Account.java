package se.liu.ida.tdp024.account.data.api.entity;

import java.io.Serializable;
import java.util.List;

public interface Account extends Serializable {

    public long getId();
            
    public String getAccountType();
    public void setAccountType(String type);
    
    public String getPersonKey();
    public void setPersonKey(String personKey);
    
    public String getBankKey();
    public void setBankKey(String bankKey);
    
    public int getHoldings();
    public void widthdrawHoldings(int holdings);
    public void depositHoldings(int holdings);
    
}
