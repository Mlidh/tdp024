package se.liu.ida.tdp024.account.data.api.facade;

import java.util.List;
import se.liu.ida.tdp024.account.data.api.entity.Account;
import se.liu.ida.tdp024.account.data.api.entity.Transaction;

public interface AccountEntityFacade {
    
    public long create(String type, String personKey, String bankKey);
    
    public List<Account> find(String name);
    
    public String debit(long accountID, int amount);
    public String credit(long accountID, int amount);

    
}
