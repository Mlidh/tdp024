package se.liu.ida.tdp024.account.data.api.facade;

import java.util.List;
import se.liu.ida.tdp024.account.data.api.entity.Transaction;

public interface TransactionEntityFacade {
    
    public long create(String type,int amount,String status,long accountID);
    
    public List<Transaction> findAllByAccount(long accountID);
    
}
