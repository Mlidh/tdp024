package se.liu.ida.tdp024.account.logic.test.facade;

import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import se.liu.ida.tdp024.account.data.api.entity.Account;
import se.liu.ida.tdp024.account.data.api.entity.Transaction;
import se.liu.ida.tdp024.account.data.api.util.StorageFacade;
import se.liu.ida.tdp024.account.data.impl.db.facade.AccountEntityFacadeDB;
import se.liu.ida.tdp024.account.data.impl.db.facade.TransactionEntityFacadeDB;
import se.liu.ida.tdp024.account.data.impl.db.util.StorageFacadeDB;
import se.liu.ida.tdp024.account.logic.api.facade.AccountLogicFacade;
import se.liu.ida.tdp024.account.logic.impl.facade.AccountLogicFacadeImpl;

public class AccountLogicFacadeTest {

    
    //--- Unit under test ---//
    public AccountLogicFacade accountLogicFacade = 
            new AccountLogicFacadeImpl(new AccountEntityFacadeDB(), new TransactionEntityFacadeDB()); 
    public StorageFacade storageFacade = new StorageFacadeDB();

    
    @After
    public void tearDown() {
        storageFacade.emptyStorage();
    }
    
    
    
    @Test
    public void testCreate() {
        {
            String name = "Marcus Bendtsen";
            String bank = "SWEDBANK";
            String accountType = "CHECK";
            long id = accountLogicFacade.create(accountType, name, bank);
            Assert.assertFalse(id == 0);
        }
        
        //Below should fail
        {
            String name = "Bendtsen";
            String bank = "SWEDBANK";
            String accountType = "CHECK";
            long id = accountLogicFacade.create(accountType, name, bank);
            Assert.assertTrue(id == 0);
        }
        {
            String name = "Marcus Bendtsen";
            String bank = "SWEDBANK";
            String accountType = "YOLO";
            long id = accountLogicFacade.create(accountType, name, bank);
            Assert.assertTrue(id == 0);
        }
                
        {
            String name = "Marcus Bendtsen";
            String bank = "INGENBANK";
            String accountType = "CHECK";
            long id = accountLogicFacade.create(accountType, name, bank);
            Assert.assertTrue(id == 0);
        }
     
    }
    
    @Test
    public void debitAndCreditTest()
    {
        {
            String name = "Marcus Bendtsen";
            String bank = "SWEDBANK";
            String accountType = "CHECK";
            long id = accountLogicFacade.create(accountType, name, bank);
            String statuscre = accountLogicFacade.credit(id, 50);
            Assert.assertTrue(statuscre == "OK");
            String status = accountLogicFacade.debit(id, 50);
            Assert.assertTrue(status == "OK");
        }
        {
            String name = "Marcus Bendtsen";
            String bank = "SWEDBANK";
            String accountType = "SAVINGS";
            long id = accountLogicFacade.create(accountType, name, bank);
            String statuscre = accountLogicFacade.credit(id, 20);
            Assert.assertTrue(statuscre == "OK");
            String status = accountLogicFacade.debit(id, 50);
            Assert.assertTrue(status == "FAILED");
        }
        {
            String statuscre = accountLogicFacade.credit(-200, 20);
            Assert.assertTrue(statuscre == "FAILED");
            String status = accountLogicFacade.debit(-500, 50);
            Assert.assertTrue(status == "FAILED");
        }
        
    }
    
    
    @Test
    public void testFind()
    {
        {
            String name = "Anders Andersson";
            List<Account> accounts = accountLogicFacade.find(name);
            Assert.assertTrue(accounts.size() == 0);
        }
        {
            String name = "Anders Andersson";
            String bank = "SWEDBANK";
            String accountType = "CHECK";
            accountLogicFacade.create(accountType, name, bank);
            accountLogicFacade.create(accountType, name, bank);
            List<Account> accounts = accountLogicFacade.find(name);
            Assert.assertTrue(accounts.size() == 2);
        }
    }
    
    @Test
    public void testfindTransactions()
    {
        
        {
            String name = "Anders Andersson";
            String bank = "SWEDBANK";
            String accountType = "CHECK";
            long account = accountLogicFacade.create(accountType, name, bank);
            List<Transaction> transaction = accountLogicFacade.transactions(account);
            Assert.assertTrue(transaction.size() == 0);
        }
        
        {
            String name = "Anders Andersson";
            String bank = "SWEDBANK";
            String accountType = "CHECK";
            long account = accountLogicFacade.create(accountType, name, bank);
            accountLogicFacade.credit(account, 500);
            accountLogicFacade.debit(account, 20);
            List<Transaction> transaction = accountLogicFacade.transactions(account);
            Assert.assertTrue(transaction.size() == 2);
        }
        
    }
    
    
}