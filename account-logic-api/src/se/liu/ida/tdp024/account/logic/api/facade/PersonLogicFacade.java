/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.liu.ida.tdp024.account.logic.api.facade;

/**
 *
 * @author Klante
 */
public interface PersonLogicFacade {
        
    public String getPersonKey(String name);
    
}
