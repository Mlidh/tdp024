package se.liu.ida.tdp024.account.logic.api.facade;

import java.util.List;
import se.liu.ida.tdp024.account.data.api.entity.Account;
import se.liu.ida.tdp024.account.data.api.entity.Transaction;


public interface AccountLogicFacade {
    
    public long create(String type, String name, String bank);
    
    public List<Account> find(String name);
    
    public String debit(long account, int amount);
    
    public String credit(long account, int amount);
    
    public List<Transaction> transactions(long account);
    
}
